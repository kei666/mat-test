//
// Created by kei666 on 17/06/28.
//

#include <opencv2/opencv.hpp>
#include <boost/timer/timer.hpp>


namespace {
    const int width = 640;
    const int height = 480;

    void TestPtr() {
        cv::Mat img = cv::Mat::zeros(width, height, CV_8UC3);
        for (int y = 0; y < img.rows; y++) {
            for (int x = 0; x < img.cols; x++) {
                img.ptr<cv::Vec3b>(y)[x][0] += 1;
                img.ptr<cv::Vec3b>(y)[x][1] += 1;
                img.ptr<cv::Vec3b>(y)[x][2] += 1;
            }
        }
    }

    void TestAt() {
        cv::Mat img = cv::Mat::zeros(width, height, CV_8UC3);
        for (int y = 0; y < img.rows; y++) {
            for (int x = 0; x < img.cols; x++) {
                img.at<cv::Vec3b>(y, x)[0] += 1;
                img.at<cv::Vec3b>(y, x)[1] += 1;
                img.at<cv::Vec3b>(y, x)[2] += 1;
            }
        }
    }

    void TestPointer() {
        cv::Mat img = cv::Mat::zeros(width, height, CV_8UC3);
        for (int y = 0; y < img.rows; y++) {
            cv::Vec3b *p = &img.at<cv::Vec3b>(y, 0);
            for (int x = 0; x < img.cols; x++) {
                (*p)[0] += 1;
                (*p)[1] += 1;
                (*p)[2] += 1;
                p++;
            }
        }
    }

    void TestIterator() {
        cv::Mat img = cv::Mat::zeros(width, height, CV_8UC3);
        cv::MatIterator_<cv::Vec3b> itr = img.begin<cv::Vec3b>();
        cv::MatIterator_<cv::Vec3b> itr_end = img.end<cv::Vec3b>();

        for (int i = 0; itr != itr_end; itr++, i++) {
            cv::Vec3b bgr = (*itr);
            (*itr)[0] += 1;
            (*itr)[1] += 1;
            (*itr)[2] += 1;
        }
    }

    void TestData() {
        cv::Mat img = cv::Mat::zeros(width, height, CV_8UC3);
        for (int y = 0; y < img.rows; y++) {
            for (int x = 0; x < img.cols; x++) {
                for (int c = 0; c < 3; c++) {
                    img.data[y * width + x * img.elemSize() + c] += 1;
                }
            }
        }
    }

    void TestforEach() {
        cv::Mat img = cv::Mat::zeros(width, height, CV_8UC1);
        img.forEach<uchar>([](uchar &p, const int *position) -> void {
            p += 1;
        });
    }
}

int main(int argc, char **argv) {
    boost::timer::cpu_timer timer;

    //ptr
    TestPtr();
    std::cout << timer.format() << std::endl;

    //at
    timer.start();
    TestAt();
    std::cout << timer.format() << std::endl;

    //pointer
    timer.start();
    TestPointer();
    std::cout << timer.format() << std::endl;

    //iterator
    timer.start();
    TestIterator();
    std::cout << timer.format() << std::endl;

    //data
    timer.start();
    TestData();
    std::cout << timer.format() << std::endl;

    //forEach
    timer.start();
    TestforEach();
    std::cout << timer.format() << std::endl;

    return EXIT_SUCCESS;
}