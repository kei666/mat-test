cmake_minimum_required(VERSION 3.5.1 FATAL_ERROR)

set(OpenCV_DIR "/usr/locals/share/OpenCV")


find_package(OpenCV REQUIRED)

if (OpenCV_FOUND)
    include_directories(${OpenCV_INCLUDE_DIRS})
    link_directories (${OpenCV_LIBS})

    message(STATUS "OpenCV version: ${OpenCV_VERSION}")
else()
    message(WARNING "Could not find OpenCV.")
endif()